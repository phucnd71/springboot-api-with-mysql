package com.bootcamp.task62.rest_api.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.task62.rest_api.model.CWard;
import com.bootcamp.task62.rest_api.service.CDistrictService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDistrictController {
    @Autowired
    CDistrictService districtService;

    @GetMapping("/wards")
    public ResponseEntity<Set<CWard>> getWards(@RequestParam(value = "districtId") int districtId) {
        try {
            Set<CWard> wards = districtService.getWardByDistrictId(districtId);
            if (wards != null && !wards.isEmpty()) {
                return new ResponseEntity<>(wards, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

}
