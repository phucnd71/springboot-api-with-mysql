package com.bootcamp.task62.rest_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.task62.rest_api.model.CProvince;

public interface CProvinceRepository extends JpaRepository<CProvince, Integer>{
}
