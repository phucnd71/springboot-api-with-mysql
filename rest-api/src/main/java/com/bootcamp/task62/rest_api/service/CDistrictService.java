package com.bootcamp.task62.rest_api.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootcamp.task62.rest_api.model.CDistrict;
import com.bootcamp.task62.rest_api.model.CWard;
import com.bootcamp.task62.rest_api.repository.CDistrictRepository;

@Service
public class CDistrictService {
    @Autowired
    CDistrictRepository districtRepository;
    public Set<CWard> getWardByDistrictId(int id) {
        CDistrict districts = districtRepository.findById(id).get();
        if(districts != null) {
            return districts.getWards();
        }else {
            return new HashSet<>();
        }
    }
}
