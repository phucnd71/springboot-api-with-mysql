package com.bootcamp.task62.rest_api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.task62.rest_api.model.CDistrict;
import com.bootcamp.task62.rest_api.model.CProvince;
import com.bootcamp.task62.rest_api.repository.CProvinceRepository;
import com.bootcamp.task62.rest_api.service.CProvinceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.data.domain.*;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProvinceController {
    @Autowired
    CProvinceService provinceService;
    @Autowired
    CProvinceRepository provinceRepository;

    @GetMapping("/provinces")
    public ResponseEntity<List<CProvince>> getMethodName() {
        try {
            return new ResponseEntity<>(this.provinceService.getProvinces(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping("/provinces5")
    public ResponseEntity<List<CProvince>> getFiveVoucher(
            @RequestParam(value = "page", defaultValue = "1") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CProvince> list = new ArrayList<CProvince>();
            provinceRepository.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceId(@RequestParam(value = "provinceId") int provinceId) {
        try {
            Set<CDistrict> districts = provinceService.getDistrictByProvinceId(provinceId);
            if (districts != null && !districts.isEmpty()) {
                return new ResponseEntity<>(districts, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

}
