package com.bootcamp.task62.rest_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.task62.rest_api.model.CDistrict;


public interface CDistrictRepository extends JpaRepository<CDistrict, Integer>{
    
}
