package com.bootcamp.task62.rest_api.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootcamp.task62.rest_api.model.CDistrict;
import com.bootcamp.task62.rest_api.model.CProvince;
import com.bootcamp.task62.rest_api.repository.CProvinceRepository;

@Service
public class CProvinceService {
    @Autowired
    private CProvinceRepository provinceRepository;
    public ArrayList<CProvince> getProvinces() {
        ArrayList<CProvince> provinces = new ArrayList<>();
        this.provinceRepository.findAll().forEach(provinces::add);
        return provinces;
    }

    public Set<CDistrict> getDistrictByProvinceId(int id) {
        CProvince provinces = this.provinceRepository.findById(id).get();
        if(provinces != null) {
            return provinces.getDistricts();
        }else{
            return new HashSet<>();
        }
    }
}
