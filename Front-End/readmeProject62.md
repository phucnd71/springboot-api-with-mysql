## ProjectTask62
## Description
 Lấy dữ liệu load vào select Province . Bắt được sự kiện thay đổi lấy Id Province để lấy District và sau đó load dữ liệu vào ô Select District.
 Lắng nghe sự kiện khi thay đổi district bắt được id District và load data Ward vào ô select.
## Feature
![Provice](images/01_province.png)
![District](images/02_district.png)
![Ward](images/03_ward.png)

### Method `getProvinces()`
Method này trả về danh sách các tỉnh. Nó tạo một ArrayList mới, sau đó thêm tất cả các tỉnh từ `provinceRepository` vào danh sách.

### Method `getDistrictByProvinceId(int id)`
Method này trả về tập hợp các quận thuộc một tỉnh có ID nhất định. Nó tìm kiếm tỉnh trong `provinceRepository` bằng ID, sau đó trả về tập hợp các quận của tỉnh đó. Nếu không tìm thấy tỉnh, nó trả về một HashSet rỗng.

### Method `getWards(@RequestParam(value = "districtId") int districtId)`
Method này trả về tập hợp các phường thuộc một quận có ID nhất định. Nó cố gắng lấy tập hợp các phường từ `districtService` bằng ID của quận. Nếu tập hợp phường không rỗng, nó trả về tập hợp đó với mã trạng thái HTTP 200 (OK). Nếu tập hợp phường rỗng, nó trả về null với mã trạng thái HTTP 404 (NOT FOUND). Nếu có lỗi xảy ra, nó trả về null với mã trạng thái HTTP 500 (INTERNAL SERVER ERROR).
## Technogogy
Front-end: Bootstrap 4, Javascript, Jquery 3, Ajax, Json,...
Back-end: Spring, Hibernate, JWT
Framework: adminLTE
DB: MySQL - PhpMyAdmin
